from django.db import models

STATUS = (
    (0,"Draft"),
    (1,"Publish")
)
# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=40)
    #thumbnails to MEDIA_ROOT/thumbnails
    thumbnail = models.FileField(upload_to="thumbnails")
    shortDescription = models.CharField(max_length=100)
    #Banners to MEDIA_ROOT/banners
    banner = models.FileField(upload_to="banners")
    description = models.TextField(default='')
    status = models.IntegerField(choices=STATUS,default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now= True)

    class Meta:
        ordering = ['-created_on']
        verbose_name_plural = "Categories"
    
    def __str__(self):
        return self.title